-- +migrate Up
CREATE TABLE "trips" (
    "id" SERIAL PRIMARY KEY,
    "origin_id" INTEGER NOT NULL,
    "destination_id" INTEGER NOT NULL,
    "dates" VARCHAR(27) NOT NULL,
    "price" INTEGER NOT NULL DEFAULT 0
);

-- +migrate Down
DROP TABLE "trips";