# Test for Paack

## How to start
```
git clone git clone https://gitlab.com/kuftachev.a/paack-challenge
cd paack-challenge
```
```
docker-compose up
```
```
# Enter to a container to make migrations:
docker exec -it pack-migrator bash

# run inside the container
sql-migrate up
```

## URLs
- [http://localhost:8080/v1/trip](http://localhost:8080/v1/trip)
- [http://localhost:8080/v1/trip/1](http://localhost:8080/v1/trip/1)
- [http://localhost:8080/v1/cities](http://localhost:8080/v1/cities)