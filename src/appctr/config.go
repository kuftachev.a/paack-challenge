package appctr

import (
	"github.com/spf13/viper"
)

func Env() string {
	return params.env
}

func HTTPAddr() string {
	return params.httpPAddr
}

func dbUrl() string {
	return params.dbUrlVar
}

const (
	envPrefix = "PACK"

	envVar    = "env"
	dbUrlVar  = "db_url"
	httpPAddr = "http_address"
)

const (
	EnvDev  = "dev"
	EnvProd = "prod"
)

var params = struct {
	env       string
	dbUrlVar  string
	httpPAddr string
}{}

var cfg viper.Viper

func prepareCfg() {
	cfg = *viper.New()
	cfg.SetEnvPrefix(envPrefix)
	cfg.AutomaticEnv()
	params.env = cfg.GetString(envVar)
	params.dbUrlVar = cfg.GetString(dbUrlVar)
	params.httpPAddr = cfg.GetString(httpPAddr)
}
