package appctr

func Start() {
	prepareCfg()
	prepareLog()
	prepareDB()
	prepareRouting()
}
