package appctr

import (
	"log"

	"go.uber.org/zap"
)

func Log() *zap.Logger {
	return &lg
}

var lg zap.Logger

func prepareLog() {
	var err error
	var l *zap.Logger

	if Env() == EnvProd {
		l, err = zap.NewProduction()
	} else {
		l, err = zap.NewDevelopment()
	}

	if err != nil {
		log.Fatalln("Logger can't be instanced")
	} else {
		lg = *l
	}
}
