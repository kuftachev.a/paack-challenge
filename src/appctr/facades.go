package appctr

func Facade() *facade {
	return &fcd
}

type facade struct {
	cityNameByID func(int32) string
}

var fcd = facade{}

func (f *facade) SetCityNameByID(fn func(int32) string) {
	if f.cityNameByID == nil {
		f.cityNameByID = fn
	}
}

func (f *facade) CityNameByID(id int32) string {
	return f.cityNameByID(id)
}
