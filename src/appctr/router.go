package appctr

import (
	routing "github.com/go-ozzo/ozzo-routing"
	"github.com/go-ozzo/ozzo-routing/content"
)

func Router() *routing.Router {
	return r
}

func RouteGroup(module string) *routing.RouteGroup {
	return r.Group(module)
}

var r *routing.Router

func prepareRouting() {
	r = routing.New()

	r.Use(
		content.TypeNegotiator(content.JSON),
	)
}
