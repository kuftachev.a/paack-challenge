package appctr

import (
	"time"

	"go.uber.org/zap"

	dbx "github.com/go-ozzo/ozzo-dbx"

	_ "github.com/jackc/pgx/stdlib"
)

func DB() *dbx.DB {
	return db
}

var db *dbx.DB

func prepareDB() {
	var d *dbx.DB
	var err error
	for {
		d, err = dbx.MustOpen("pgx", dbUrl())
		if err != nil {
			Log().Error("Failed to open db: ", zap.Error(err))
			time.Sleep(5 * time.Second)
			continue
		}
		break
	}

	if Env() == EnvDev {
		d.LogFunc = Log().Sugar().Debugf
	}

	go pingForStayConnected(d)

	db = d

	Log().Debug("Database is connected")
}

func pingForStayConnected(d *dbx.DB) {
	for {
		time.Sleep(5 * time.Minute)
		if err := d.DB().Ping(); err != nil {
			Log().Error("db ping error", zap.Error(err))
		}
	}
}
