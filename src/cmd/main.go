package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/kuftachev.a/paack-challenge/src/appctr"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/trips"
	"go.uber.org/zap"
)

func main() {
	appctr.Start()
	appctr.Log().Info("App has started")

	startModules()
	appctr.Log().Info("Modules have started")

	stopServer := make(chan os.Signal, 1)
	done := make(chan struct{}, 1)
	signal.Notify(stopServer, syscall.SIGINT, syscall.SIGTERM)

	serve(stopServer, done)

	<-done
}

func serve(stop <-chan os.Signal, done chan<- struct{}) {
	srv := &http.Server{
		Addr:    appctr.HTTPAddr(),
		Handler: appctr.Router(),
	}

	go func() {
		appctr.Log().Info("Starting the HTTP server")
		appctr.Log().Fatal("Serve error", zap.Error(srv.ListenAndServe()))
	}()

	<-stop

	appctr.Log().Info("The HTTP server is shutting down")
	srv.Shutdown(context.Background())
	appctr.Log().Info("The HTTP server has shuted down")

	done <- struct{}{}
}

func startModules() {
	r := appctr.RouteGroup("/v1")
	res := geo.Start(geo.Params{Router: r.Group("/cities")})
	appctr.Facade().SetCityNameByID(res.CityNameByID)
	_ = trips.Start(trips.Params{Router: r.Group("/trip"), DB: appctr.DB()})
}
