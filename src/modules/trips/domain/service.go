package domain

type Service interface {
	TripByID(int32) (*Trip, error)
	Trips() ([]*Trip, error)
	AddTrip(*Trip) error
}

func NewService(rp Repository) Service {
	return &service{rp: rp}
}

type service struct {
	rp Repository
}

func (s service) TripByID(id int32) (*Trip, error) {
	return s.rp.TripByID(id)
}

func (s service) Trips() ([]*Trip, error) {
	return s.rp.Trips()
}

func (s service) AddTrip(t *Trip) error {
	return s.rp.AddTrip(t)
}
