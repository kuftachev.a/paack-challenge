package domain

import "gitlab.com/kuftachev.a/paack-challenge/src/appctr"

type Trip struct {
	id            int32
	originID      int32
	destinationID int32
	dates         string
	price         int32
}

func NewTrip(
	id int32,
	originID int32,
	destinationID int32,
	dates string,
	price int32,
) *Trip {
	return &Trip{
		id:            id,
		originID:      originID,
		destinationID: destinationID,
		dates:         dates,
		price:         price,
	}
}

func (t *Trip) ID() int32 {
	return t.id
}

func (t *Trip) OriginID() int32 {
	return t.originID
}

func (t *Trip) DestinationID() int32 {
	return t.destinationID
}

func (t *Trip) Price() int32 {
	return t.price
}

func (t *Trip) Dates() string {
	return t.dates
}

func (t *Trip) Origin() string {
	return appctr.Facade().CityNameByID(t.originID)
}

func (t *Trip) Destination() string {
	return appctr.Facade().CityNameByID(t.destinationID)
}
