package domain

type Repository interface {
	TripByID(int32) (*Trip, error)
	Trips() ([]*Trip, error)
	AddTrip(*Trip) error
}
