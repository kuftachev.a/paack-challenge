package infrastructure

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/domain"
)

func NewRepository(db *dbx.DB) domain.Repository {
	return &repository{db: db}
}

type repository struct {
	db *dbx.DB
}

func (r repository) TripByID(id int32) (*domain.Trip, error) {
	dto := &dto{}

	if err := r.db.Select().Model(id, dto); err != nil {
		return nil, err
	}

	return tripFromDTO(dto), nil
}

func (r repository) Trips() ([]*domain.Trip, error) {
	dtos := make([]dto, 0)

	if err := r.db.Select().From(tripsTable).All(&dtos); err != nil {
		return nil, err
	}

	return tripsFromDTOs(dtos), nil
}

func (r repository) AddTrip(t *domain.Trip) error {
	return r.db.Model(dtoFromTrip(t)).Exclude(tripsIDField).Insert()
}
