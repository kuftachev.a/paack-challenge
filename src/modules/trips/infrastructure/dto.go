package infrastructure

import "gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/domain"

const (
	tripsTable   = "trips"
	tripsIDField = "id"
)

type dto struct {
	ID            int32  `json:"id"`
	OriginID      int32  `json:"origin_id"`
	DestinationID int32  `json:"destination_id"`
	Dates         string `json:"dates"`
	Price         int32  `json:"price"`
}

func (dto) TableName() string {
	return tripsTable
}

func dtoFromTrip(t *domain.Trip) *dto {
	return &dto{
		ID:            t.ID(),
		OriginID:      t.OriginID(),
		DestinationID: t.DestinationID(),
		Dates:         t.Dates(),
		Price:         t.Price(),
	}
}

func tripFromDTO(d *dto) *domain.Trip {
	return domain.NewTrip(
		d.ID,
		d.OriginID,
		d.DestinationID,
		d.Dates,
		d.Price,
	)
}

func tripsFromDTOs(dtos []dto) []*domain.Trip {
	trips := make([]*domain.Trip, 0, len(dtos))

	for _, d := range dtos {
		trips = append(trips, tripFromDTO(&d))
	}

	return trips
}
