package application

import "gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/domain"

type tripReadVM struct {
	Origin      string  `json:"origin"`
	Destination string  `json:"destination"`
	Dates       string  `json:"dates"`
	Price       float32 `json:"price"`
}

func readVMFromTrip(t *domain.Trip) tripReadVM {
	return tripReadVM{
		Origin:      t.Origin(),
		Destination: t.Destination(),
		Dates:       t.Dates(),
		Price:       float32(t.Price()) / 100,
	}
}

func readVMsFromTrips(ts []*domain.Trip) []tripReadVM {
	vms := make([]tripReadVM, 0, len(ts))

	for _, c := range ts {
		vms = append(vms, readVMFromTrip(c))
	}

	return vms
}

type tripWriteVM struct {
	OriginID      int32   `json:"originId"`
	DestinationID int32   `json:"destinationId"`
	Dates         string  `json:"dates"`
	Price         float32 `json:"price"`
}

func tripFromWriteVM(vm *tripWriteVM) *domain.Trip {
	return domain.NewTrip(
		0,
		vm.OriginID,
		vm.DestinationID,
		vm.Dates,
		int32(vm.Price)*100,
	)
}
