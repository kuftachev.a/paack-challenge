package application

import (
	"strconv"

	routing "github.com/go-ozzo/ozzo-routing"
	"gitlab.com/kuftachev.a/paack-challenge/src/appctr"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/domain"
	"go.uber.org/zap"
)

type Handler interface {
	Trips(ctx *routing.Context) error
	TripByID(ctx *routing.Context) error
	AddTrip(ctx *routing.Context) error
}

func NewHandler(srv domain.Service) Handler {
	return &handler{srv: srv}
}

type handler struct {
	srv domain.Service
}

func (h handler) Trips(ctx *routing.Context) error {
	trips, err := h.srv.Trips()
	if err != nil {
		return err
	}

	if err := ctx.Write(readVMsFromTrips(trips)); err != nil {
		appctr.Log().Error("", zap.Error(err))
	}

	return nil
}

func (h handler) TripByID(ctx *routing.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return err
	}

	trip, err := h.srv.TripByID(int32(id))
	if err != nil {
		return err
	}

	if err := ctx.Write(readVMFromTrip(trip)); err != nil {
		appctr.Log().Error("", zap.Error(err))
	}

	return nil
}

func (h handler) AddTrip(ctx *routing.Context) error {
	vm := &tripWriteVM{}

	if err := ctx.Read(&vm); err != nil {
		return err
	}

	return h.srv.AddTrip(tripFromWriteVM(vm))
}
