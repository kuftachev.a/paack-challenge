package trips

import (
	dbx "github.com/go-ozzo/ozzo-dbx"
	routing "github.com/go-ozzo/ozzo-routing"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/application"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/domain"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/trips/infrastructure"
)

type Params struct {
	Router *routing.RouteGroup
	DB     *dbx.DB
}

type Result struct{}

func Start(p Params) Result {
	rp := infrastructure.NewRepository(p.DB)

	srv := domain.NewService(rp)

	h := application.NewHandler(srv)

	addRoutes(p.Router, h)

	return Result{}
}

func addRoutes(r *routing.RouteGroup, h application.Handler) {
	r.Get(``, h.Trips)
	r.Post(``, h.AddTrip)
	r.Get(`/<id:\d+>`, h.TripByID)
}
