package geo

import (
	routing "github.com/go-ozzo/ozzo-routing"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/application"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/domain"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/infrastructure"
)

type Params struct {
	Router *routing.RouteGroup
}

type Result struct {
	CityNameByID func(int32) string
}

func Start(p Params) Result {
	rp := infrastructure.NewRepository()

	srv := domain.NewService(rp)

	h := application.NewHandler(srv)

	f := application.NewFacade(srv)

	addRoutes(p.Router, h)

	return Result{
		CityNameByID: f.CityNameByID,
	}
}

func addRoutes(r *routing.RouteGroup, h application.Handler) {
	r.Get(``, h.Cities)
}
