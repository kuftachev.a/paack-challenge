package infrastructure

import (
	"bufio"
	"os"

	"gitlab.com/kuftachev.a/paack-challenge/src/appctr"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/domain"
	"go.uber.org/zap"
)

func NewRepository() domain.Repository {
	return &repository{cities: parseFile()}
}

type repository struct {
	cities []string
}

func (r repository) CityByID(id int32) *domain.City {
	return domain.NewCity(id, r.cities[id-1])
}

func (r repository) Cities() []*domain.City {
	cities := make([]*domain.City, 0, len(r.cities))

	for i, v := range r.cities {
		cities = append(cities, domain.NewCity(int32(i), v))
	}

	return cities
}

func parseFile() []string {
	var lines []string

	file, err := os.Open("cities.txt")
	if err != nil {
		appctr.Log().Fatal("cities file can't be read:", zap.Error(err))
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines
}
