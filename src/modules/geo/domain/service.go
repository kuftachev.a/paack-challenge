package domain

type Service interface {
	CityByID(int32) *City
	Cities() []*City
}

func NewService(rp Repository) Service {
	return &service{rp: rp}
}

type service struct {
	rp Repository
}

func (s service) CityByID(id int32) *City {
	return s.rp.CityByID(id)
}

func (s service) Cities() []*City {
	return s.rp.Cities()
}
