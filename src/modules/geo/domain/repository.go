package domain

type Repository interface {
	CityByID(int32) *City
	Cities() []*City
}
