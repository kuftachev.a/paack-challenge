package domain

type City struct {
	id   int32
	name string
}

func NewCity(id int32, name string) *City {
	return &City{id: id, name: name}
}

func (c *City) ID() int32 {
	return c.id
}

func (c *City) Name() string {
	return c.name
}
