package application

import (
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/domain"
)

type Facade interface {
	CityNameByID(int32) string
}

func NewFacade(srv domain.Service) Facade {
	return &facade{srv: srv}
}

type facade struct {
	srv domain.Service
}

func (f facade) CityNameByID(id int32) string {
	return f.srv.CityByID(id).Name()
}
