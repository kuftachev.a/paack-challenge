package application

import (
	routing "github.com/go-ozzo/ozzo-routing"
	"gitlab.com/kuftachev.a/paack-challenge/src/appctr"
	"gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/domain"
	"go.uber.org/zap"
)

type Handler interface {
	Cities(ctx *routing.Context) error
}

func NewHandler(srv domain.Service) Handler {
	return &handler{srv: srv}
}

type handler struct {
	srv domain.Service
}

func (h handler) Cities(ctx *routing.Context) error {
	cities := h.srv.Cities()

	if err := ctx.Write(vmsFromCities(cities)); err != nil {
		appctr.Log().Error("", zap.Error(err))
	}

	return nil
}
