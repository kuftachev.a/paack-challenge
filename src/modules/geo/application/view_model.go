package application

import "gitlab.com/kuftachev.a/paack-challenge/src/modules/geo/domain"

type cityVM struct {
	ID   int32  `json:"id"`
	Name string `json:"name"`
}

func vmFromCity(c *domain.City) cityVM {
	return cityVM{
		ID:   c.ID(),
		Name: c.Name(),
	}
}

func vmsFromCities(cs []*domain.City) []cityVM {
	vms := make([]cityVM, 0, len(cs))

	for _, c := range cs {
		vms = append(vms, vmFromCity(c))
	}

	return vms
}
