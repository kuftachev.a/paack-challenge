#!/bin/sh

if [ "$PACK_GO_VENDOR" = true ]
then
	echo "Starting vendoring..."
	cd /src/src && go mod vendor	
fi

if [ "$PACK_GO_DEBUG" = true ]
then
	echo "Starting app with debbuger"
	cd /src/src && go build -gcflags="all=-N -l" -o /dst/server -mod vendor ./cmd && cd /dst && dlv --listen=:40000 --headless=true --api-version=2 --accept-multiclient exec ./server
fi

if [ "$PACK_GO_DEBUG" != true ]
then
	echo "Starting app"
	cd /src/src && go build -o /dst/server -mod vendor ./cmd && cd /dst && ./server
fi
